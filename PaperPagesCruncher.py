# -*- coding: utf-8 -*-
import os

from PublicationCruncher import PublicationCruncher

class PaperPagesCruncher(PublicationCruncher):

    def __init__(self):
        PublicationCruncher.__init__(self)

        self.publication_name = "paper"
        self.publicationDir = ""
        self._processedPapers = []
        self._excludePaths = []
        self._excludeList = []
        self._jinja_template_file = "jinja_papers.html"
        self.load_jinja()


    def _assign_values(self):
        self._assign_data_now()
        self._collect_cds_info(self.publication_dict["Ref Code"])

    def doFormatting(self):
        """
        collects all the data and manages to start every needed operation for the publication
        """
        for paper in self.reader:

            self.publication_dict = paper
            if not self.publication_dict["Ref Code"]:
                continue
            # we keep track of papers that have been already processed
            # to be able to filter among the "extra papers"
            self._processedPapers.append(self.publication_dict["Ref Code"])

            # if user wants a single paper to be updated
            # we check if it's the correct ref code. Otherwise we continue
            if isinstance(self.run_on, basestring):
                if self.run_on != "all":
                    if self.run_on != self.publication_dict["Ref Code"]:
                        continue

            if self.publication_dict["Ref Code"].upper() in self._excludeList:
                continue

            self.publication_path = os.path.join(self.base_path, self.publicationDir, self.publication_dict["Ref Code"])

            self._embargo_operations()

            if not self.publication_needs_update():
                continue

            print("processing paper: %s " % self.publication_dict["Ref Code"])

            self.collect_images()
            self.convert_images()
            self._assign_values()
            self._create_jinja_page()


    def processCSV(self):

        self.set_reader()
        self.doFormatting()


    def processExtraPapers(self):

        extra_papers_path = self.base_path + self.publicationDir

        for item in os.listdir(extra_papers_path):
            if os.path.isdir(extra_papers_path + item):
                if item not in self._processedPapers:
                    if item not in self._excludePaths and item not in self._excludeList:
                        self.publication_path = extra_papers_path + item + "/"

                        self.publication_dict = {}
                        self.publication_dict["Ref Code"] = item

                        # if user wants a single paper to be updated
                        # we check if it's the correct ref code. Otherwise we continue
                        if isinstance(self.run_on, basestring):
                            if self.run_on != "all":
                                if self.run_on != self.publication_dict["Ref Code"]:
                                    continue

                        self._embargo_operations()

                        if not self.publication_needs_update():
                            continue

                        print("processing extra paper: %s " % self.publication_dict["Ref Code"])

                        self.collect_images()
                        self.convert_images()
                        self._assign_values()
                        self._create_jinja_page()
