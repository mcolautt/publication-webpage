#!/usr/bin/env python
"""
This script gathers informations about
ATLAS papers, confnotes, pubnotes, plots and combined results
to create ATLAS public pages

# author: Colautti Maurizio - maurizio.colautti@cern.ch
"""
# -*- coding: utf-8 -*-

import json
import os
import traceback
import argparse
import time
import datetime

from PaperPagesCruncher import PaperPagesCruncher

print ("Execution starts at %s" % datetime.datetime.now())
START = time.time()

# to make it correctly run when called from another path as symlink
os.chdir(os.path.dirname(os.path.realpath(__file__)))

PARSER = argparse.ArgumentParser()
PARSER.add_argument('--PAPERS', dest='paper_ref', default=False)
PARSER.add_argument("--CDS", dest="forceCDS", action='store_true', default=False)
PARSER.add_argument("--SANDBOX", dest="sandbox", default=False)
ARGS = PARSER.parse_args()

SETTING_JSON_FILE = "settings.json"
if ARGS.sandbox:
    SETTING_JSON_FILE = ARGS.sandbox

with open(SETTING_JSON_FILE, "r") as setting_file:
    SETTING_TEXT = setting_file.read()
SETTINGS = json.loads(SETTING_TEXT)

if ARGS.paper_ref:
    BUILD = PaperPagesCruncher()
    try:
        if BUILD.generic_settings_tag in SETTINGS:
            for key, value in SETTINGS[BUILD.generic_settings_tag][0].items():
                setattr(BUILD, key, value)
        if BUILD.publication_name in SETTINGS:
            for key, value in SETTINGS[BUILD.publication_name][0].items():
                setattr(BUILD, key, value)
        BUILD.set_run_on(ARGS.paper_ref)
        if ARGS.forceCDS:
            BUILD.set_force_cds(ARGS.forceCDS)
        if not ARGS.sandbox:
            BUILD.processCSV()
        BUILD.processExtraPapers()
    except:
        print ("****** >> %s" % (traceback.format_exc()))
        os.system("echo 'Error in PaperPagesCruncher\n%s' | mail -s 'Error in createPages.py / public atlas pages' mcolautt " % (traceback.format_exc().replace("'","\"")))


END = time.time()
print("Execution time: ")
print(END - START)
